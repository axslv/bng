/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3.elmi;

import java.util.List;
import lv.nmc.entities.n3.RegistrationEntry;

/**
 *
 * @author jm
 */
public interface RegistrationMapper {
    List<RegistrationEntry> searchUser(RegistrationEntry re);
    
    List<RegistrationEntry> courseDetailsByName(RegistrationEntry re);
    
    List<RegistrationEntry> courseDetailsByDomain(RegistrationEntry re);
    
    List<RegistrationEntry> coursesList(RegistrationEntry re);
    
    List<RegistrationEntry> coursePrices(RegistrationEntry re);
    List<RegistrationEntry> selectCompanies(RegistrationEntry re);
}
