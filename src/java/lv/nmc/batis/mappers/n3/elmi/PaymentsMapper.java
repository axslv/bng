/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.batis.mappers.n3.elmi;

import java.util.List;
import lv.nmc.entities.n3.Payment;

/**
 *
 * @author jm
 */
public interface PaymentsMapper {
    
    List<Payment> clientsByDateEndInterval(Payment pt);
    
    List<Payment> debtsByCompany(Payment pt);

    List<Payment> clientDetails(Payment pt);

    List<Payment> debtsByClientData(Payment pt);

    List<Payment> debtsByClientId(Payment pt);

    List<Payment> paymentDetailsById(Payment pt);

    List<Payment> advancesByUserData(Payment pt);

    List<Payment> advancesByUserId(Payment pt);

    List<Payment> stuckDebtsByDateInterval(Payment pt);

    List<Payment> clientsByDateEnd(Payment pt);

    void _insertPayment(Payment pt);

    void _insertAdvance(Payment pt);

    void _updateHideAdvance(Payment pt);

    void _updateEraseAdvance(Payment pt);

    void _updateHidePayment(Payment pt);
    
     void _updateBillNumber(Payment pt);

}
