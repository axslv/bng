/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.webs.n3.elmi;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import lv.nmc.dao.n3.elmi.PaymentDao;
import lv.nmc.entities.n3.Payment;
import lv.nmc.xext.XReader;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import org.apache.commons.beanutils.BeanUtils;

@UrlBinding("/1c/PaymentService.rs")
public class paymentsActionBean implements ActionBean, ValidationErrorHandler {

    private ActionBeanContext context;
    private String xmlOut = "";
    private Payment payment = new Payment();
    private String event = "noEvent";
    private List<Integer> courseId = new ArrayList<Integer>();
    private List<String> courseNames = new ArrayList<String>();
    private List<String> departmentNames = new ArrayList<String>();

    @DefaultHandler
    @HandlesEvent("dbRun")
    public Resolution executeSelect() {
        prepareBean();
        PaymentDao sd = new PaymentDao();

        if (event.contains("_update")) {
            sd.executeUpdate(event, payment);
        } else if (event.contains("_delete")) {
            sd.executeUpdate(event, payment);
        } else if (event.contains("_insert")) {
            sd.executeUpdate(event, payment);
        } else {
            sd.executeSelect(event, payment);
        }

        setXmlOut(new XReader(Payment.class).object2xml(sd.getResults()));
        return new ForwardResolution("/WEB-INF/views/payment.jsp");

    }

    protected void prepareBean() {
        if (!"dbRun".equals(context.getEventName())) {
            return;
        }

        try {
            BeanUtils.populate(payment, context.getRequest().getParameterMap());
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {

            ex.printStackTrace();
        }
    }

    @Override
    public Resolution handleValidationErrors(ValidationErrors ve) throws Exception {
        return new ForwardResolution("/WEB-INF/views/n3/payment.jsp");
    }

    /**
     * @return the context
     */
    @Override
    public ActionBeanContext getContext() {
        return context;
    }

    /**
     * @param context the context to set
     */
    @Override
    public void setContext(ActionBeanContext context) {
        this.context = context;
    }

    /**
     * @return the xmlOut
     */
    public String getXmlOut() {
        return xmlOut;
    }

    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

    /**
     * @param xmlOut the xmlOut to set
     */
    public void setXmlOut(String xmlOut) {
        this.xmlOut = xmlOut;
    }

}
