/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.nmc.xext;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 *
 * @author mj
 */
public class XReader {
    private XStream xs = new XStream(new StaxDriver());
    private final Class cls;
    
    
    public XReader(Class cls) {
        this.cls = cls;        
    }
           

    public String object2xml(Object ob) {
        xs.processAnnotations(cls);
        return xs.toXML(ob);
    }
    
    
    
}
