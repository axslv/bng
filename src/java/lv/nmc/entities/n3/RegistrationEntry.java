package lv.nmc.entities.n3;



import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@XStreamAlias("RegistrationEntry")
public class RegistrationEntry implements Serializable {
    private Integer registrationId, groupId, companyId, paymentId;
   
    private Integer userId;
    private Integer courseId, pvn=0;
    private String nameSurname, company, contractNr;
    private String personCode;
    private String dateStart, dateEnd;
    private String dateC;
    private String courseName, domain, groupAbbr, courseDescription, coursePrice, coursePrice1, coursePrice2;

    private String event, title1c;




    
    public RegistrationEntry() {
        
    }

    /**
     * @return the registrationId
     */
    public Integer getRegistrationId() {
        return registrationId;
    }

    /**
     * @param registrationId the registrationId to set
     */
    public void setRegistrationId(Integer registrationId) {
        this.registrationId = registrationId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the courseId
     */
    public Integer getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the nameSurname
     */
    public String getNameSurname() {
        return nameSurname;
    }

    /**
     * @param nameSurname the nameSurname to set
     */
    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    /**
     * @return the personCode
     */
    public String getPersonCode() {
        return personCode;
    }

    /**
     * @param personCode the personCode to set
     */
    public void setPersonCode(String personCode) {
        this.personCode = personCode;
    }

    /**
     * @return the dateStart
     */
    public String getDateStart() {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

  
    /**
     * @return the dateC
     */
    public String getDateC() {
        return dateC;
    }

    /**
     * @param dateC the dateC to set
     */
    public void setDateC(String dateC) {
        this.dateC = dateC;
    }

    /**
     * @return the groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }


    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }


    /**
     * @return the dateEnd
     */
    public String getDateEnd() {
        return dateEnd;
    }

    /**
     * @param dateEnd the dateEnd to set
     */
    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }


    /**
     * @return the event
     */
    public String getEvent() {
        return event;
    }

    /**
     * @param event the event to set
     */
    public void setEvent(String event) {
        this.event = event;
    }

  

    /**
     * @return the companyId
     */
    public Integer getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId the companyId to set
     */
    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

  

  

    /**
     * @return the paymentId
     */
    public Integer getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

  

    /**
     * @return the contractNr
     */
    public String getContractNr() {
        return contractNr;
    }

    /**
     * @param contractNr the contractNr to set
     */
    public void setContractNr(String contractNr) {
        this.contractNr = contractNr;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @return the groupAbbr
     */
    public String getGroupAbbr() {
        return groupAbbr;
    }

    /**
     * @param groupAbbr the groupAbbr to set
     */
    public void setGroupAbbr(String groupAbbr) {
        this.groupAbbr = groupAbbr;
    }

    /**
     * @return the courseDescription
     */
    public String getCourseDescription() {
        return courseDescription;
    }

    /**
     * @param courseDescription the courseDescription to set
     */
    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    /**
     * @return the pvn
     */
    public Integer getPvn() {
        return pvn;
    }

    /**
     * @param pvn the pvn to set
     */
    public void setPvn(Integer pvn) {
        this.pvn = pvn;
    }

    /**
     * @return the coursePrice
     */
    public String getCoursePrice() {
        return coursePrice;
    }

    /**
     * @param coursePrice the coursePrice to set
     */
    public void setCoursePrice(String coursePrice) {
        this.coursePrice = coursePrice;
    }

    /**
     * @return the coursePrice1
     */
    public String getCoursePrice1() {
        return coursePrice1;
    }

    /**
     * @param coursePrice1 the coursePrice1 to set
     */
    public void setCoursePrice1(String coursePrice1) {
        this.coursePrice1 = coursePrice1;
    }

    /**
     * @return the coursePrice2
     */
    public String getCoursePrice2() {
        return coursePrice2;
    }

    /**
     * @param coursePrice2 the coursePrice2 to set
     */
    public void setCoursePrice2(String coursePrice2) {
        this.coursePrice2 = coursePrice2;
    }

    /**
     * @return the title1c
     */
    public String getTitle1c() {
        return title1c;
    }

    /**
     * @param title1c the title1c to set
     */
    public void setTitle1c(String title1c) {
        this.title1c = title1c;
    }




}
